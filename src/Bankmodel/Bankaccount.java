package Bankmodel;

/**
 * Created by sabiut on 15/03/19.
 */

public class Bankaccount {

    private int accountNumber;
    private double balance;
    private String customerName;
    private String email;
    private int phoneNumber;



    public Bankaccount(int accountNumber,double balance,String customerName,String email,int phoneNumber){
        this.accountNumber=accountNumber;
        this.balance=balance;
        this.customerName=customerName;
        this.email=email;
        this.phoneNumber=phoneNumber;
    }

    public int getAccountNumber(){
        return accountNumber;

    }

    public double getBalance(){
        return balance;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public String getCustomerName(){
        return customerName;
    }



    public void setAccountNumber(int accountNumber){
        this.accountNumber=accountNumber;

    }

    public void setBalance(double balance){
        this.balance=balance;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber=phoneNumber;
    }

    public void setCustomerName(String customerName){
        this.customerName=customerName;
    }

    public double deposit(double depositAmount){
        this.balance+=depositAmount;
        System.out.println(" you have successfully made a deposit of "+ depositAmount + " VT" + " to the account " + accountNumber +
                "\n" + " your new balance is "+ this.balance +"VT");

        return depositAmount;

    }
    public double widthraw(double withdrawAmount){
        if(balance-withdrawAmount<0){
            System.out.println(" You have insuffecient Fund");
        }else{
            this.balance=this.balance-withdrawAmount;
            System.out.println("You've withrow:" + withdrawAmount+ " VT" + " from your account, Your new balanc is:" + this.balance+ " VT");

        }

        return balance;

    }

    public void print(){
        System.out.println("Bank Account Details");
        System.out.println("======================");
        System.out.println("Account Number:"+ accountNumber +" \n" + "Customer Name:"+ customerName+ " \n" +"Current Balance:"+balance+ " \n"+ "Contact Details:"+ phoneNumber);;
        System.out.println("#######################\n");

    }

}
